var express = require("express")
var bodyparser = require("body-parser")
const app = express()
app.use(bodyparser.urlencoded({ extended: true }))
var studentR = require("./Routes/studentRouter")

app.get("/", function (req, res) {
    res.sendfile("./home.html")
})

app.use("/student", studentR)

app.listen(7000, function (req, res) {
    console.log("Listening")
})