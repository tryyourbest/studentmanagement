var express = require('express');
const studentRouter = express.Router();
var mongodb = require('mongodb');
var monclient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017"

studentRouter.get("/", function (req, res) {
    res.sendfile("./studentManager.html")
})

studentRouter.get("/new", function (req, res) {
    res.sendfile("./sNew.html")
})

studentRouter.post("/new/result", function (req, res) {
    var id = req.body.ids
    var name = req.body.sname
    var usn = req.body.susn
    var branch = req.body.sbranch
    console.log(name, usn, branch)

    monclient.connect(url, function (err, databases) {
        if (err) {
            console.log(err)
        }
        else {
            var dtb = databases.db("students")
            var stdtable = dtb.collection("student")
            var data = { _id: id, stdname: name, stdusn: usn, stdbranch: branch }
            stdtable.insert(data, function (err, result) {
                if (err) {
                    res.send("Insertion failed")
                }
                else {
                    res.send(result)
                }
            })
        }
    })
})

studentRouter.get("/edit", function (req, res) {
    res.sendfile("./sEdit.html")
})

studentRouter.post("/edit/done", function (req, res) {
    var uid = req.body.uids
    var uname = req.body.unames
    var uusn = req.body.uusns
    var ubranch = req.body.ubranchs
    console.log(uid, uname, uusn, ubranch)

    monclient.connect(url, function (err, databases) {
        if (err) {
            console.log(err)
        }
        else {
            var dtb = databases.db("students")
            var stdtable = dtb.collection("student")

            stdtable.update({ _id: uid }, { $set: { stdname: uname, stdusn: uusn, stdbranch: ubranch } }, function (err, result) {
                if (err) {
                    res.send("Updation failed")
                }
                else {
                    res.send(result)
                }
            })
        }
    })
})

studentRouter.get("/delete", function (req, res) {
    res.sendfile("./sDelete.html")
})

studentRouter.post("/delete/done", function (req, res) {
    var deleted = req.body.deletion
    console.log(deleted)

    monclient.connect(url, function (err, databases) {
        if (err) {
            console.log(err)
        }
        else {
            var dtb = databases.db("students")
            var stdtable = dtb.collection("student")
            var data = { _id: deleted }
            stdtable.deleteOne(data, function (err, result) {
                if (err) {
                    res.send("Deletion failed")
                }
                else {
                    res.send(result)
                }
            })
        }
    })
})

studentRouter.get("/view", function (req, res) {
    res.sendfile("./sView.html")
})

studentRouter.post("/view/done", function (req, res) {
    var views = req.body.view
    console.log(views)

    monclient.connect(url, function (err, databases) {
        if (err) {
            console.log(err)
        }
        else {
            var dtb = databases.db("students")
            var stdtable = dtb.collection("student")

            stdtable.find({ stdname: views }).toArray(function (err, result) {
                if (err) {
                    res.send("Failed")
                }
                else {
                    res.send(result)
                }
            })
        }
    })
})

module.exports = studentRouter;